import {InjectionToken} from '@angular/core';
import {NbErrorTooltipModuleConfig} from './nb-error-tooltip.module';

export const NB_ERROR_TOOLTIP_CONFIG = new InjectionToken<NbErrorTooltipModuleConfig>('NB_ERROR_TOOLTIP_CONFIG');
