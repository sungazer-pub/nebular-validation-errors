# NebularValidationErrors

This package automatically shows validation errors in forms built with Nebular [https://akveo.github.io/nebular/](https://akveo.github.io/nebular/)

It comes with a configurable error normalizer that allows to configure what is shown in the error tooltip.

## Example

See here: https://sungazer-pub.gitlab.io/nebular-validation-errors

## Usage

Import the `NbErrorTooltipModule` in the module where you want to use the `nbErrorTooltip` directive.

Apply the `nbErrorTooltip` directive to `nbInput` components and add some validation requirements to the element.

```angular2html
<input [(ngModel)]="formData.field3" minlength="3" name="field3" 
nbErrorTooltip nbInput placeholder="Min length(3)" required>
```

Now, whenever the form control is touched or its value changes and validation fails, a validation tooltip should pop up to notify you.

## How to customize the error messages

By default, this package only provides a simple english message, but you can hook your own message creation logic.

Create a new service that implements the `NbErrorNormalizerInterface`. In the `normalize` method you can hook whatever logic you need (translations, etc...) to show your error message. If you want to show the default value, just return null.

```typescript
import {Injectable} from '@angular/core';
import {NbErrorNormalizerInterface, NbErrorNormalizerResult} from '@sungazer/nebular-validation-errors';

@Injectable({
  providedIn: 'root'
})
export class ErrorNormalizerService implements NbErrorNormalizerInterface {

  constructor() {
  }

  normalize(errors: any): NbErrorNormalizerResult {
    if (errors.required) {
      return {message: 'This field is required'};
    }
    return null;
  }
}
```

After creating the service, register it as a provider in the AppModule.

```typescript
import {NB_ERROR_NORMALIZER} from '@sungazer/nebular-validation-errors';

@NgModule({
  declarations: [
    //...
  ],
  imports: [
    //...
  ],
  providers: [
    {provide: NB_ERROR_NORMALIZER, useClass: ErrorNormalizerService},
  ],
  //...
})
export class AppModule {
}

```

## How to customize the tooltip placement

Tooltip placement and adjustment (see Nebular docs for details) can be customized in 2 places:
- At the module level (import the module using the `withConfig` helper, i.e. `NbErrorTooltipModule.withConfig({...}))`
- At the single directive level (use `nbErrorTooltipPosition` and `nbErrorTooltipAdjustent` attributes)
