import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {LazyRoutingModule} from './lazy-routing.module';
import {LazyPage} from './pages/lazy/lazy.page';
import {NbButtonModule, NbCardModule, NbInputModule, NbLayoutModule, NbPosition} from '@nebular/theme';
import {FormsModule} from '@angular/forms';
import {NbErrorTooltipModule} from '@sungazer/nebular-validation-errors';


@NgModule({
  declarations: [LazyPage],
  imports: [
    CommonModule,
    LazyRoutingModule,
    NbLayoutModule,
    NbCardModule,
    FormsModule,
    NbInputModule,
    NbButtonModule,
    NbErrorTooltipModule.withConfig({
      position: NbPosition.LEFT
    })
  ]
})
export class LazyModule {
}
