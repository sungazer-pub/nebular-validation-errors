import { Component } from '@angular/core';
import {NbPosition} from '@nebular/theme';
import {FormControl, NgControl, NgForm} from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'nebular-validation-errors';

}
